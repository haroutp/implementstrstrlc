﻿using System;

namespace ImplementStrStr
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            string needle = "ll";
            string haystack = "hello";

            System.Console.WriteLine(StrStr(haystack, needle));


        }

        public static int StrStr(string haystack, string needle) {
            if(haystack.Contains(needle)){
                return haystack.IndexOf(needle);
            }else{
                return -1;
            }
        }
    }
}
